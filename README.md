#MXKTabBarController

This project implements a very basic custom tab bar controller.
The motivation behind this project is to play with Core Animation and have custom transitions between tabs.
The code is written in Swift. 