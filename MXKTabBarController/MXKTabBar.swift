//
//  MXKTabBar.swift
//  MXKTabBarController
//
//  Created by Max Korytko on 2014-12-15.
//  Copyright (c) 2014 Max Korytko. All rights reserved.
//

import UIKit

protocol MXKTabBarDataSource {
    func numberOfTabs() -> Int
    func createViewControllerForTabAtIndex(tabIndex: Int) -> UIViewController
}

class MXKTabBar: UIView {
    private var tabs = [MXKTab]()
    var dataSource: MXKTabBarDataSource?
    var selectTabHandler: ((MXKTab) -> Void)?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override class func requiresConstraintBasedLayout() -> Bool {
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        handleTouch(touches.anyObject() as? UITouch)
    }
    
    private func handleTouch(touch: UITouch?) {
        if touch == nil || touch?.tapCount > 1 {
            return
        }
        
        var touchPoint = touch!.locationInView(self)
        var touchTab = tabAtPoint(touchPoint)
        
        if touchTab != nil {
            selectTab(touchTab!)
        }
    }
    
    private func tabAtPoint(point: CGPoint) -> MXKTab? {
        for tab in self.tabs {
            if tab.pointInside(tab.convertPoint(point, fromView: self), withEvent: nil) {
                return tab
            }
        }
        
        return nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        initializeTabs()
    }
    
    func initializeTabs() {
        if self.tabs.count > 0 {
            return
        }
        
        createTabs()
        layoutTabs()
        selectFirstTab()
    }
    
    private func createTabs() {
        if self.dataSource == nil {
            return
        }
        
        for tabIndex in 0..<self.dataSource!.numberOfTabs() {
            self.tabs.append(createTabAtIndex(tabIndex))
            self.addSubview(self.tabs[tabIndex])
        }
    }
    
    private func createTabAtIndex(tabIndex: Int) -> MXKTab {
        let tab = MXKTab(viewController: self.dataSource!.createViewControllerForTabAtIndex(tabIndex))
        
        tab.index = tabIndex
        
        return tab
    }
    
    private func layoutTabs() {
        if self.tabs.count == 0 {
            return
        }
        
        var constraints = [AnyObject]()
        var lastTab: MXKTab?
        
        for tab in self.tabs {
            tab.backgroundColor = tab.viewController!.view.backgroundColor
            tab.setTranslatesAutoresizingMaskIntoConstraints(false)
            let views: Dictionary<NSObject, AnyObject> = lastTab == nil ? ["tab":tab] : ["tab":tab, "lastTab":lastTab!]
            
            constraints += NSLayoutConstraint.constraintsWithVisualFormat(
                lastTab == nil ? "H:|[tab]" : "H:[lastTab]-[tab(==lastTab)]",
                options: NSLayoutFormatOptions(0),
                metrics: nil,
                views: views)
            
            constraints += NSLayoutConstraint.constraintsWithVisualFormat(
                "V:|[tab]|",
                options: NSLayoutFormatOptions(0),
                metrics: nil,
                views: views)
            
            lastTab = tab
        }
        
        constraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "H:[lastTab]|",
            options: NSLayoutFormatOptions(0),
            metrics: nil,
            views: ["lastTab":lastTab!])
        
        self.addConstraints(constraints)
    }
    
    private func selectFirstTab() {
        if self.tabs.count > 0 {
            selectTab(self.tabs[0])
        }
    }
    
    private func selectTab(tab: MXKTab) {
        if self.selectTabHandler != nil {
            self.selectTabHandler!(tab)
        }
    }
}
