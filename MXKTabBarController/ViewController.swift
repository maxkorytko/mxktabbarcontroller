//
//  ViewController.swift
//  MXKTabBarController
//
//  Created by Max Korytko on 2014-12-15.
//  Copyright (c) 2014 Max Korytko. All rights reserved.
//

import UIKit

class ViewController: MXKTabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func numberOfTabs() -> Int {
        return 3
    }
    
    override func createViewControllerForTabAtIndex(tabIndex: Int) -> UIViewController {
        let viewController = UIViewController()
        
        switch tabIndex {
        case 0: viewController.view.backgroundColor = UIColor.redColor()
        case 1: viewController.view.backgroundColor = UIColor.greenColor()
        case 2: viewController.view.backgroundColor = UIColor.blueColor()
        default: viewController.view.backgroundColor = UIColor.whiteColor()
        }
        
        return viewController
    }
    
    override func animateTransitionFromTab(currentTab: MXKTab, toTab newTab: MXKTab) {
        let animationDuration = 0.25
        var transition = CATransition()
        
        transition.startProgress = 0.0
        transition.endProgress = 1.0
        transition.duration = animationDuration
        
        let fromView = currentTab.viewController?.view
        let toView = newTab.viewController?.view
        
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        fromView?.layer.addAnimation(transition, forKey: "transition")
        
        toView?.hidden = false
        fromView?.hidden = true
        
        fromView?.removeFromSuperviewAfterDelay(animationDuration)
    }
}