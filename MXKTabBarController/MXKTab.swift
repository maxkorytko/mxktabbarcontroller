//
//  MXKTab.swift
//  MXKTabBarController
//
//  Created by Max Korytko on 2014-12-15.
//  Copyright (c) 2014 Max Korytko. All rights reserved.
//

import UIKit

class MXKTab: UIView {
    let viewController: UIViewController?
    var index = -1
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewController: UIViewController!) {
        self.viewController = viewController
        super.init(frame: CGRectZero)
    }
}
