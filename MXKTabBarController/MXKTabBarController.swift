//
//  MXKTabBarController.swift
//  MXKTabBarController
//
//  Created by Max Korytko on 2014-12-15.
//  Copyright (c) 2014 Max Korytko. All rights reserved.
//

import UIKit

class MXKTabBarController: UIViewController, MXKTabBarDataSource {
    let tabBar: MXKTabBar
    var selectedTab: MXKTab?
    
    required init(coder aDecoder: NSCoder) {
        self.tabBar = MXKTabBar(frame: CGRectZero)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildTabBar()
        layoutTabBar()
    }
    
    private func buildTabBar() {
        self.tabBar.dataSource = self
        self.tabBar.selectTabHandler = selectTab
        
        self.view.addSubview(self.tabBar)
    }
    
    private func layoutTabBar() {
        self.tabBar.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
            "H:|[tabBar]|",
            options: NSLayoutFormatOptions(0),
            metrics: nil,
            views: ["topLayoutGuide":self.topLayoutGuide, "tabBar":self.tabBar]))
        
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
            "V:[topLayoutGuide][tabBar(==64)]",
            options: NSLayoutFormatOptions(0),
            metrics: nil,
            views: ["topLayoutGuide":self.topLayoutGuide, "tabBar":self.tabBar]))
        
        self.view.layoutIfNeeded()
    }
    
    private func selectTab(tab: MXKTab) {
        if self.selectedTab === tab {
            return
        }
        
        transitionFromTab(self.selectedTab, toTab: tab)
        self.selectedTab = tab
    }
    
    private func transitionFromTab(currentTab: MXKTab?, toTab newTab: MXKTab) {
        self.addTabContent(newTab)
        
        if currentTab != nil {
            self.view.bringSubviewToFront(currentTab!.viewController!.view)
            animateTransitionFromTab(currentTab!, toTab: newTab)
        }
    }
    
    private func addTabContent(tab: MXKTab) {
        if let viewController = tab.viewController {
            viewController.view.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            self.view.addSubview(viewController.view)
            
            let views = ["contentView":viewController.view, "tabBar":self.tabBar]
            
            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(
                "H:|[contentView]|",
                options: NSLayoutFormatOptions(0),
                metrics: nil,
                views: views))
            
            self.view.addConstraints(NSLayoutConstraint .constraintsWithVisualFormat(
                "V:[tabBar][contentView]|",
                options: NSLayoutFormatOptions(0),
                metrics: nil,
                views: views))
        }
    }
    
    /*
     * Subclasses may override this method in order to provide animated transition between tabs.
     */
    func animateTransitionFromTab(currentTab: MXKTab, toTab newTab: MXKTab) {
        currentTab.viewController?.view.removeFromSuperview()
    }
    
    // MARK: MXKTabBarDataSource
    
    func numberOfTabs() -> Int {
        return 0
    }
    
    func createViewControllerForTabAtIndex(tabIndex: Int) -> UIViewController {
        return UIViewController(nibName: nil, bundle: nil)
    }
}
