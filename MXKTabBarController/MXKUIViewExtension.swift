//
//  MXKUIViewExtension.swift
//  MXKTabBarController
//
//  Created by Max Korytko on 2014-12-17.
//  Copyright (c) 2014 Max Korytko. All rights reserved.
//

import UIKit

extension UIView {
    func removeFromSuperviewAfterDelay(delay: Double) {
        dispatch_after(
            dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))),
            dispatch_get_main_queue(),
            {
                self.removeFromSuperview()
            }
        )
    }
}
